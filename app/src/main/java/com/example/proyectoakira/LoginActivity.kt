package com.example.proyectoakira

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import com.example.proyectoakira.databinding.ActivityLoginBinding
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_registro_usuario_log.*
import java.util.concurrent.Executors

class LoginActivity : AppCompatActivity() {
    val flatActivo=1
    val flatNoActivo=0

    //Login Activity=ActivityLoginBinding
    lateinit var binding: ActivityLoginBinding


    private val appDatabase by lazy {
        AppDatabase.obtenerInstanciaBD(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_login)

        binding=ActivityLoginBinding.inflate(layoutInflater)


        setContentView(binding.root)
        //validar
        validar()
        recuperarpreferencia()
        //1. Boton autenticar
        binding.button3.setOnClickListener {
            autenticar()

        }
        binding.btnnuevo.setOnClickListener {
            val intent = Intent(this,RegistroUsuarioLog::class.java)
            startActivity(intent)
        }

    }

    private fun validar() {
        edtid.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (edtid.text.toString().length<10){
                    edtid.error = "9 caracteres como minimo"
                }
            }
        })
        edtid.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (edtclave.text.toString().length<6){
                    edtclave.error = "6 caracteres como minimo"
                }
            }
        })
    }

    private fun autenticar() {
        //2. guardar funcion
        //creamos variables
        val idusuario = binding.edtapellidos.text.toString()
        val contrasenia = binding.edtclave.text.toString()



        Executors.newSingleThreadExecutor().execute{
            val respuesta=   appDatabase.usuarioDao().autenticar(idusuario,contrasenia)
            if (respuesta==0){
                runOnUiThread {
                    Toast.makeText(this,"Credenciales invalidas",Toast.LENGTH_SHORT).show()
                    // return@runOnUiThread
                }

            }else{
                //Existe el usuario
                if (chkconforme.isChecked) guardarpreferencia(idusuario, contrasenia,flatActivo)
                else guardarpreferencia("","",flatNoActivo)

                irMenuPrincipal()
            }
        }

    }

    private fun irMenuPrincipal() {
        val intent=Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    private fun guardarpreferencia(idusuario: String, contrasenia: String,flatActivo: Int) {
        getSharedPreferences(Constantes.PREFERENCIA_LOGIN,0).edit().apply {
            putString(Constantes.KEY_EMAIL,idusuario)
            putString(Constantes.KEY_CONTRASENIA,contrasenia)
            putInt(Constantes.KEY_ESTADO_CHECKBOX,flatActivo)
            apply() //grabar preferencia
        }

    }
    private fun recuperarpreferencia(){
        val preferencia =getSharedPreferences(Constantes.PREFERENCIA_LOGIN,0) //recuperando
        edtapellidos.setText(preferencia.getString(Constantes.KEY_EMAIL,""))
        edtclave.setText(preferencia.getString(Constantes.KEY_CONTRASENIA,""))

        val flatEstado = preferencia.getInt(Constantes.KEY_ESTADO_CHECKBOX,flatNoActivo)

        chkconforme.isChecked = flatEstado==1
    }
}

