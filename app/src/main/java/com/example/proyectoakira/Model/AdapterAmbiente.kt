package com.example.proyectoakira.Model

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.example.proyectoakira.R

class AdapterAmbiente(private val context: Context, private val ambientes: MutableList<ModelAmbiente>): BaseAdapter()  {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val inflater = LayoutInflater.from(context)
        val container = inflater.inflate(R.layout.row_ambiente, null)
        val titulo = container.findViewById<TextView>(R.id.txtnombreambiente)
        val cantidad = container.findViewById<TextView>(R.id.txtcantidadambiente)
        val imgamb=container.findViewById<ImageView>(R.id.image1)
        //Extraer la entidad
        val ambienteEntity = ambientes[position]

        //Asociar la entidad con el XML
        titulo.text=ambienteEntity.titulo
        cantidad.text=ambienteEntity.cantidad
        imgamb.setImageResource(ambienteEntity.imagen)

        return container
    }

    override fun getItem(position: Int)= ambientes[position]

    override fun getItemId(position: Int):Long=0

    override fun getCount():Int = ambientes.size
}