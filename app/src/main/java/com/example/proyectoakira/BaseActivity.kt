package com.example.proyectoakira

import androidx.appcompat.app.AppCompatActivity
import android.content.Intent
import android.os.Bundle


abstract class BaseActivity:AppCompatActivity() {

    protected fun next(activityClass: Class<*>, bundle: Bundle?, destroy: Boolean) {
        val intent = Intent(this, activityClass)
        bundle?.let {
            intent.putExtras(it)
        }
        startActivity(intent)
        if(destroy){
            finish()
        }
    }
}