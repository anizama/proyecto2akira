package com.example.proyectoakira.Fragmentos

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.proyectoakira.Model.AdapterAmbiente
import com.example.proyectoakira.Model.ModelAmbiente
import com.example.proyectoakira.R
import kotlinx.android.synthetic.main.fragment_main_content_ambientes.*


class MainContentAmbientes : Fragment() {
    // TODO: Rename and change types of parameters
  /*  private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }
*/
    private var listener : OnAmbienteListener?=null
    private  var ambientes = mutableListOf<ModelAmbiente>()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_content_ambientes, container, false)
    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnAmbienteListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnAmbienteListener")
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setData()
        context?.let {
            lstAmbientes.adapter= AdapterAmbiente(it,ambientes)
        }
        lstAmbientes.setOnItemClickListener{ _,_,i,_ ->
            listener?.let {
                it.onSelectedItemContact(ambientes[i])
            }
        }
    }

    private fun setData() {
        val ambiente1 = ModelAmbiente("Piso1","20",R.drawable.ic_launcher_background)
        val ambiente2 = ModelAmbiente("Piso1","20",R.drawable.ic_launcher_background)
        val ambiente3 = ModelAmbiente("Piso1","20",R.drawable.ic_launcher_background)
        val ambiente4 = ModelAmbiente("Piso1","20",R.drawable.ic_launcher_background)

        ambientes.add(ambiente1)
        ambientes.add(ambiente2)
        ambientes.add(ambiente3)
        ambientes.add(ambiente4)


    }

    private fun first(): ModelAmbiente? {
        ambientes?.let {
            return it[0]
        }
        return null
    }
    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    fun getUrlFromIntent() {
        val url = "www.google.com"
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        startActivity(intent)
    }

}