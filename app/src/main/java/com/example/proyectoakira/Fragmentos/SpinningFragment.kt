package com.example.proyectoakira.Fragmentos

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.proyectoakira.AppDatabase
import com.example.proyectoakira.Model.ModelAmbiente
import com.example.proyectoakira.R
import com.example.proyectoakira.adapterlugares.LugaresAdapter
import com.example.proyectoakira.databinding.FragmentSpinningBinding
import kotlinx.android.synthetic.main.fragment_spinning.*


class SpinningFragment : Fragment() {
        //21.12
    private lateinit var binding:FragmentSpinningBinding
    private val appDatabase by lazy { activity?.let { AppDatabase.obtenerInstanciaBD(it) } }
    val lugares = mutableListOf<ModelAmbiente>()
    lateinit var adaptador:LugaresAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        binding=FragmentSpinningBinding.inflate(layoutInflater,container,false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cargarinformacion()
         configuraradaptador()
    }

    private fun configuraradaptador(){
        adaptador= LugaresAdapter(lugares)
        recyclerviewlugares.adapter=adaptador
        recyclerviewlugares.layoutManager=LinearLayoutManager(requireContext())
    }

    //21.12
    private fun cargarinformacion() {
        lugares.add(ModelAmbiente("Sala de Ejercicios Cardio"," Aforo de 20 personas",R.drawable.cardiogym))
        lugares.add(ModelAmbiente("Sala de Maquinas","Aforo de 30 personas",R.drawable.machinegym))
        lugares.add(ModelAmbiente("Sala de clases grupales","Aforo de 25 personas",R.drawable.grupalgym))
        lugares.add(ModelAmbiente("Sala de ventas","Aforo de 10 personas",R.drawable.ventasgym))
    }

}