package com.example.proyectoakira.Fragmentos

import com.example.proyectoakira.Ambiente
import com.example.proyectoakira.Model.ModelAmbiente


interface OnAmbienteListener {
    fun onSendMessage(msg:String)
    fun onSelectedItemContact(ambienteEntity: ModelAmbiente)
    fun renderfirst(ambienteEntity: ModelAmbiente)
}