package com.example.proyectoakira.Fragmentos

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.proyectoakira.AppDatabase
import com.example.proyectoakira.FragmentsTime.DatePickerFragment
import com.example.proyectoakira.FragmentsTime.TimePickerFragment
import com.example.proyectoakira.NinioInscrito
import com.example.proyectoakira.R
import com.example.proyectoakira.UsuarioInscrito
import com.example.proyectoakira.databinding.FragmentKidsBinding

import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.fragment_kids.*
import kotlinx.android.synthetic.main.fragment_muscles.*
import java.lang.Exception
import java.util.concurrent.Executors


class KidsFragment : Fragment() {
    private lateinit var binding: FragmentKidsBinding
    //private val appDatabase by lazy { activity?.let { AppDatabase.obtenerInstanciaBD(it) } }
    private val appDatabase by lazy { activity?.let { AppDatabase.obtenerInstanciaBD(it) } }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding=FragmentKidsBinding.inflate(layoutInflater,container,false)
        return binding.root

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val datePicker=DatePickerFragment { day, month, year -> onDateSelected(day,month,year) }
        datePicker.show(requireFragmentManager(),"datePicker")
        val timePicker= TimePickerFragment{onTimeSelected(it)}
        timePicker.show(requireFragmentManager(),"time")
        Sbtnseparar.setOnClickListener {
            Toast.makeText(requireContext(),"se ha registrado correctamente",Toast.LENGTH_SHORT).show()
        registrarninioinscrito()
        }

}

    fun onDateSelected(day:Int,month:Int,year:Int){
        fechaninio.setText("Has seleccionado el dia $day de $month del $year")
    }
    //19.12
    private fun onTimeSelected(time:String){
        horaninio.setText("Has seleccionado las $time")
    }

    private fun registrarninioinscrito() {
        try {
            val nombremenor=nombreninio.text.toString()
            val deporteninio=deporteninio.text.toString()
            val edadninio=edadninio.text.toString()
            val fechaninio=fechaninio.text.toString()
            val horaninio=horaninio.text.toString()

            if (nombremenor.isNullOrEmpty() ||deporteninio.isNullOrEmpty() ||fechaninio.isNullOrEmpty()||horaninio.isNullOrEmpty()||edadninio.isNullOrEmpty())
            Toast.makeText(requireContext(),"Ingrese su nombre",Toast.LENGTH_SHORT).show()

           val ninioInscrito=NinioInscrito(0,nombremenor,deporteninio,edadninio,fechaninio,horaninio)
            Executors.newSingleThreadExecutor().execute {
            appDatabase?.ninioinscritoDao()?.insertarninioinscrito(ninioInscrito)
            }
        }catch (ex:Exception){


        }
    }
}