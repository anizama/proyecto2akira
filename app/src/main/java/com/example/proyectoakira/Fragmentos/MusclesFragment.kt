package com.example.proyectoakira.Fragmentos

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.proyectoakira.AppDatabase
import com.example.proyectoakira.FragmentsTime.DatePickerFragment
import com.example.proyectoakira.FragmentsTime.TimePickerFragment
import com.example.proyectoakira.R
import com.example.proyectoakira.UsuarioInscrito
import com.example.proyectoakira.databinding.FragmentMusclesBinding
import kotlinx.android.synthetic.main.fragment_kids.*
import kotlinx.android.synthetic.main.fragment_muscles.*
import java.lang.Exception
import java.util.concurrent.Executors


class MusclesFragment : Fragment() {
    private lateinit var binding: FragmentMusclesBinding
    private val appDatabase by lazy { activity?.let { AppDatabase.obtenerInstanciaBD(it) } }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        //20.12
      //  hora123.setOnClickListener {
        //    showDatePickerDialog()
       // }
        
       // val datePicker=DatePickerFragment { day, month, year -> onDateSelected(day,month,year) }
        //datePicker.show(requireFragmentManager(),"datePicker")
        //intento 19.12
      //  val timePicker= TimePickerFragment{onTimeSelected(it)}
        //timePicker.show(requireFragmentManager(),"time")

        binding=FragmentMusclesBinding.inflate(layoutInflater,container,false)
        return binding.root
       // return inflater.inflate(R.layout.fragment_muscles, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val datePicker=DatePickerFragment { day, month, year -> onDateSelected(day,month,year) }
        datePicker.show(requireFragmentManager(),"datePicker")
        //intento 19.12
        val timePicker= TimePickerFragment{onTimeSelected(it)}
        timePicker.show(requireFragmentManager(),"time")
        SepararVacante.setOnClickListener {
           Toast.makeText(requireContext(),"se ha registrado correctamente",Toast.LENGTH_SHORT).show()
            inscribiruser()
        }
    }
    fun onDateSelected(day:Int,month:Int,year:Int){
        hora123.setText(" $day/$month/$year")
    }
    //19.12
    private fun onTimeSelected(time:String){
        fecha123.setText("$time")
    }
    private fun inscribiruser() {
        try {
            val nombremayor=abcd.text.toString()
            val deportemayor=abcde.text.toString()
            val fechamayor=fecha123.text.toString()
            val horamayor=hora123.text.toString()

         //   if (nombremayor.isNullOrEmpty() ||deportemayor.isNullOrEmpty() ||fechamayor.isNullOrEmpty()||horamayor.isNullOrEmpty())
           //     Toast.makeText(requireContext(),"Ingrese su nombre",Toast.LENGTH_SHORT).show()

            val usuarioInscrito= UsuarioInscrito(0,nombremayor,deportemayor,fechamayor,horamayor)
            Executors.newSingleThreadExecutor().execute {
                appDatabase?.clienteinscritoDao()?.insertarclienteinscrito(usuarioInscrito)
            }
        }catch (ex: Exception){


        }
    }
    }

    //20.12
   // private fun showDatePickerDialog() {
    //val datePicker=DatePickerFragment { day, month, year -> onDateSelected(day,month,year) }
    //datePicker.show(requireFragmentManager(),"datePicker")
    //}



