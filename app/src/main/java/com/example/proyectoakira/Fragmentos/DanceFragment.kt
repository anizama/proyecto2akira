package com.example.proyectoakira.Fragmentos

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.MediaController
import androidx.fragment.app.Fragment
import com.example.proyectoakira.AppDatabase
import com.example.proyectoakira.R
import com.example.proyectoakira.databinding.FragmentDanceBinding

import kotlinx.android.synthetic.main.fragment_dance.*


class DanceFragment :Fragment(){

    private lateinit var binding: FragmentDanceBinding
    private val appDatabase by lazy { activity?.let { AppDatabase.obtenerInstanciaBD(it) } }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {


        binding= FragmentDanceBinding.inflate(layoutInflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
     /*   val mediaController= MediaController(requireContext())
        mediaController.setAnchorView(video1)
        val onlineUri= Uri.parse("https://www.youtube.com/watch?v=c3rSOHBBH88")
        //val offlineUri=Uri.parse("android.resource://$packageName/${R.raw.videogym}")
        video1.setMediaController(mediaController)
        video1.setVideoURI(onlineUri)
        video1.requestFocus()
        video1.start()

    }*/
}}


