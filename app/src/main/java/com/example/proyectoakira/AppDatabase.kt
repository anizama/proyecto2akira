package com.example.proyectoakira

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.proyectoakira.database.*
import java.util.concurrent.Executors


@Database(
    entities = [Usuario::class, Ambiente::class, Categoria::class,UsuarioInscrito::class,NinioInscrito::class],
    version = 1,
    exportSchema = true
)
abstract class AppDatabase : RoomDatabase() {

    //Matricular a todos los daos
    abstract fun usuarioDao(): UsuarioDao
    abstract fun ambientesDao(): AmbientesDao
    abstract fun categoriaDao(): CategoriaDao
    abstract fun clienteinscritoDao(): ClienteInscritoDao
    abstract fun ninioinscritoDao(): NinoInscritoDao


    companion object{

        private var instancia : AppDatabase? = null

        fun obtenerInstanciaBD(context:Context) : AppDatabase {

            if(instancia == null){
                //Creala
                instancia = Room.databaseBuilder(
                    context,
                    AppDatabase::class.java,
                    "bdgymdo")
                    .addCallback(object : RoomDatabase.Callback(){

                        override fun onCreate(db: SupportSQLiteDatabase) {
                            super.onCreate(db)

                            Executors.newSingleThreadExecutor().execute {

                                val categorias = mutableListOf<Categoria>()
                                categorias.add(Categoria(1,"SI"))
                                categorias.add(Categoria(2,"NO"))

                                instancia?.categoriaDao()?.insertarCategorias(categorias)

                                val ambientes = mutableListOf<Ambiente>()

                                ambientes.add(Ambiente(1,"Sala de spinning","Turnos MAÑANA-NOCHE","27 bicicletas","",1))
                                ambientes.add(Ambiente(2,"Sala de Baile","40 plazas disponibles","40 plazas","",1))
                                ambientes.add(Ambiente(3,"Sala de recreacion niños","Climbing","10 niños como maximo","",1))
                                ambientes.add(Ambiente(4,"Sala de cardio y Circuits","Ubicado en el primer piso","20 personas como maximo","",1))
                                ambientes.add(Ambiente(5,"Sala de Maquinas","Ubicado en el segundo y tercer piso","25 personas como maximo en cada piso","",1))

                                instancia?.ambientesDao()?.insertar(ambientes)
                            }
                        }
                }).build()
            }
            //Devuelvela
            return instancia as AppDatabase
        }

    }
}