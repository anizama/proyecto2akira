package com.example.proyectoakira

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tablaAmbientes")
data class Ambiente(

    @PrimaryKey
    @NonNull
    val codigo:Int,

    @ColumnInfo(name="nombreAmbiente")
    val nombreAmbiente:String,

    @ColumnInfo(name="descripcion")
    val descripcion:String,

    @ColumnInfo(name="Cantidad")
    val cantidad:String,

    @ColumnInfo(name="imagen")
    val imagen:String,

    @ColumnInfo(name="categoria")
    val categoria:Int
)
