package com.example.proyectoakira

class Constantes {

    companion object{

        const val PREFERENCIA_LOGIN : String = "PREFERENCIA_LOGIN"
        const val KEY_EMAIL: String = "KEY_EMAIL"
        const val KEY_CONTRASENIA: String = "KEY_CONTRASENIA"
        const val KEY_ESTADO_CHECKBOX: String = "KEY_ESTADO_CHECKBOX"

        const val KEY_CATEGORIA = "key_categoria"

    }


}