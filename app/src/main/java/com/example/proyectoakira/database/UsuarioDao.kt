package com.example.proyectoakira.database

import androidx.room.*
import com.example.proyectoakira.Usuario

@Dao
interface UsuarioDao {


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertarUsuario(usuario: Usuario)

    @Query("select count(*) from tablaUsuario where apellidos=:apellidosInput and clave=:claveInput")
    fun autenticar(apellidosInput:String, claveInput:String) : Int

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateUsuario(usuario: Usuario)

    @Delete
    fun deleteUsuario(usuario: Usuario)



}