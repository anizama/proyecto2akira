package com.example.proyectoakira.database

import androidx.room.Dao
import androidx.room.Insert
import com.example.proyectoakira.Categoria

@Dao
interface CategoriaDao {

    @Insert
    fun insertarCategorias(categorias: MutableList<Categoria>)
}