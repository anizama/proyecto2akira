package com.example.proyectoakira.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.proyectoakira.NinioInscrito

@Dao
interface NinoInscritoDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertarninioinscrito(ninioInscrito: NinioInscrito)
    @Query("select*from tablaNiniosInscritos")
    fun obtenerninosinscritos():MutableList<NinioInscrito>
}