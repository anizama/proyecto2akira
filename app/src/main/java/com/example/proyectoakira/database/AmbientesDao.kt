package com.example.proyectoakira.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.proyectoakira.Ambiente


@Dao
interface AmbientesDao {

    @Insert
    fun insertar(ambiente:MutableList<Ambiente> )

    @Query("select *from tablaAmbientes where categoria=:categoriaInput")
    fun obtenerAmbientes(categoriaInput:Int) : MutableList<Ambiente>
}