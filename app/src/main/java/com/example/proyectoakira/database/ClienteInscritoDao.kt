package com.example.proyectoakira.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.proyectoakira.UsuarioInscrito

@Dao
interface ClienteInscritoDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)

    fun insertarclienteinscrito(usuarioinscrito: UsuarioInscrito)
    @Query("select * from tablaUsuarioInscritos")
    fun obtenerusuariosinscritos():MutableList<UsuarioInscrito>


}