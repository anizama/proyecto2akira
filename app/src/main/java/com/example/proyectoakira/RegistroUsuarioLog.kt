package com.example.proyectoakira

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_registro_usuario_log.*
import java.util.concurrent.Executors
import kotlin.system.exitProcess

class RegistroUsuarioLog : AppCompatActivity() {
    val i= Intent(Intent.ACTION_DIAL)
    val appDatabase: AppDatabase by lazy {
        AppDatabase.obtenerInstanciaBD(this)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro_usuario_log)
        //1.boton cerrar
        btnclose.setOnClickListener {
            exitProcess (0);
        }
        //1. Evento llamada
        btnllamada.setOnClickListener {
           // val i= Intent(Intent.ACTION_DIAL)
            i.data= Uri.parse("tel:989097396")
            startActivity(intent)

        }
        //1.Evento del boton registrar
        btnregistrar.setOnClickListener {
            registrarcliente()
        }

        //1. validar campo id
        editTextTextPersonName5.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (editTextTextPersonName5.text.toString().length<10){
                    editTextTextPersonName5.error = "9 caracteres como minimo"
                }
            }
        })
        // validar el campo clave
        edtnumeroclave.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (edtnumeroclave.text.toString().length<7){
                    edtnuevousuario.error = "como minimo 6 caracteres"
                }
            }
        })
        //validar el campo apellido
        edtapellidonuevo.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (edtapellidonuevo.text.toString().length<10){
                    edtapellidonuevo.error = "como minimo 10 caracteres"
                }
            }
        })

    }

    private fun llamada() {
        val i= Intent(Intent.ACTION_DIAL)
        i.data= Uri.parse("tel:989097396")
        startActivity(intent)
    }

    private fun registrarcliente() {
        try{

            val nombres = editTextTextPersonName5.text.toString()
            val apellidos = edtapellidonuevo.text.toString()
            val clave = edtnumeroclave.text.toString()
            val distrito = if (rbtSB.isChecked) rbtSB.text.toString() else rdtOtros.text.toString()
            val oferta = if (rdt1.isChecked) rdt1.text.toString() else if (rdt2.isChecked) rdt2.text.toString() else  rdt3.text.toString()

            val usuario = Usuario(0,nombres,apellidos,clave,distrito, oferta)

            Executors.newSingleThreadExecutor().execute {

                appDatabase.usuarioDao().insertarUsuario(usuario)

                runOnUiThread {
                    Toast.makeText(this,"Usuario grabado correctamente!!", Toast.LENGTH_SHORT).show()
                }
            }



        }catch (ex:Exception){
            Toast.makeText(this,ex.toString(), Toast.LENGTH_SHORT).show()
        }


    }
}