package com.example.proyectoakira.adapterlugares

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.proyectoakira.Model.ModelAmbiente
import com.example.proyectoakira.R
import kotlinx.android.synthetic.main.item_lugares.view.*

class LugaresAdapter(var lugares: MutableList<ModelAmbiente>):RecyclerView.Adapter<LugaresAdapter.LugaresAdapterViewHolder>() {
    //crear clase interna ViewHolder
    class LugaresAdapterViewHolder(itemView:View):RecyclerView.ViewHolder(itemView){
    fun bind(ambiente: ModelAmbiente){
        itemView.txtnombrelugar.text=ambiente.titulo
        itemView.txtcantidadlugar.text=ambiente.cantidad
    }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LugaresAdapterViewHolder {
        val view:View=LayoutInflater.from(parent.context).inflate(R.layout.item_lugares,parent,false)
        return LugaresAdapterViewHolder(view)
    }

    override fun getItemCount(): Int {
       return lugares.size
    }

    override fun onBindViewHolder(holder: LugaresAdapterViewHolder, position: Int) {
        val lugar=lugares[position]
        holder.bind(lugar)
    }
}