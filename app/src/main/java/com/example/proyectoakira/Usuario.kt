package com.example.proyectoakira

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tablaUsuario")
data class Usuario(

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "codigo")
    val codigo:Int ,

    @ColumnInfo(name = "nombres")
    val nombres:String,

    @ColumnInfo(name = "apellidos")
    val apellidos:String,

    @ColumnInfo(name = "clave")
    val clave:String,

    @ColumnInfo(name = "distrito")
    val distrito:String,

    @ColumnInfo(name = "oferta")
    val oferta:String
)
