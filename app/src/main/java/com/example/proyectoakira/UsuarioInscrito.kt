package com.example.proyectoakira

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.sql.Time
import java.util.*

@Entity(tableName = "tablaUsuarioInscritos")
data class UsuarioInscrito (
        @PrimaryKey(autoGenerate = true)
        @NonNull
        @ColumnInfo(name = "codigo")
        val codigoid:Int ,

        @ColumnInfo(name = "nombre")
        val valornombre:String,


        @ColumnInfo(name = "edad")
        val edad:String,

        @ColumnInfo(name = "fecha")
        val fecha:String,

        @ColumnInfo(name = "hora")
        val hora:String

)
