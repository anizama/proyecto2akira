package com.example.proyectoakira

import android.graphics.Color
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.proyectoakira.Fragmentos.DanceFragment
import com.example.proyectoakira.Fragmentos.KidsFragment
import com.example.proyectoakira.Fragmentos.MusclesFragment
import com.example.proyectoakira.Fragmentos.SpinningFragment
import kotlinx.android.synthetic.main.activity_navegacion_tabs_akira.*
import java.time.temporal.TemporalAdjusters
import java.time.temporal.TemporalAdjusters.next

class MainActivity : AppCompatActivity(),  View.OnClickListener { //intento view.OnClicListener
    //intento
    private val FRAGMENT_SPINNING = 0
    private val FRAGMENT_DANCE = 1
    private val FRAGMENT_MUSCLES = 2
    private val FRAGMENT_KIDS = 3
    private val indicatorViews= mutableListOf<View>()
    private val titleViews= mutableListOf<TextView>()
    private var currentIndicator = -1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navegacion_tabs_akira)

        indicatorViews.add(iviSpinning)
        indicatorViews.add(iviDance)
        indicatorViews.add(iviMuscles)
        indicatorViews.add(iviKids)

        titleViews.add(tviSpinning)
        titleViews.add(tviDance)
        titleViews.add(tviMuscles)
        titleViews.add(tviKids)

        titleViews.forEach {
            it.setOnClickListener(this)
        }

        selectFirst()
    }

    //Posicion uno por defecto
    private fun selectFirst() {
        val bundle = Bundle()
        val fragmentId = FRAGMENT_SPINNING
        updateUI(fragmentId)
        changeFragment(bundle, fragmentId)
    }

    //1.
    override fun onClick(view: View?) {
        val bundle=Bundle()
        var fragmentId = when(view?.id){
            R.id.tviSpinning->  FRAGMENT_SPINNING
            R.id.tviDance->  FRAGMENT_DANCE
            R.id.tviMuscles->  FRAGMENT_MUSCLES
            R.id.tviKids->  FRAGMENT_KIDS
            else -> FRAGMENT_SPINNING
        }
        //2.Debemos crear funciones para los tabs actualizar y cambiar
        updateUI(fragmentId)
        changeFragment(bundle,fragmentId)
    }

    //Actualizar la posicion de fragmentos mediante el indicador
    private fun updateUI(fragmentId: Int) {
        if (currentIndicator >= 0) {
            indicatorViews[currentIndicator].setBackgroundColor(Color.TRANSPARENT)
            titleViews[currentIndicator].inputType = Typeface.NORMAL
        }
        indicatorViews[fragmentId].setBackgroundColor(Color.parseColor("#ffeb3b"))
        titleViews[fragmentId].inputType = Typeface.BOLD
        currentIndicator = fragmentId
    }

    //2.1 Funcion cambiar fragmento
    private fun changeFragment(bundle: Bundle, fragmentId: Int) {
        val fragment = factoryFragment(bundle, fragmentId)
        //fragment.setArguments(bundle);

        fragment?.let {
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.flayContainer, it)
            transaction.addToBackStack(null)

            // Commit the transaction
            transaction.commit()
        }
    }

    //Funcion para ubicary setear los fragmentos
    private fun factoryFragment(bundle: Bundle, fragmentId: Int): Fragment? {
        when (fragmentId) {
            FRAGMENT_SPINNING -> return SpinningFragment()
            FRAGMENT_DANCE -> return DanceFragment()
            FRAGMENT_MUSCLES -> return MusclesFragment()
            FRAGMENT_KIDS -> return KidsFragment()
        }
        return null
    }
    }


