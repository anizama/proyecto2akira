package com.example.proyectoakira

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tablaCategorias")
data class Categoria(

    @PrimaryKey
    @NonNull
    val codigo:Int,

    @ColumnInfo(name = "descripcion")
    val descripcion:String
)